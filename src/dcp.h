#pragma once

#include <stdio.h>

struct MPCameraCalibration {
        float color_matrix_1[9];
        float color_matrix_2[9];
        float forward_matrix_1[9];
        float forward_matrix_2[9];
        unsigned short illuminant_1;
        unsigned short illuminant_2;
        unsigned int hue_sat_map_dims[3];
        size_t tone_curve_length;
        float *tone_curve;
        float *hue_sat_map_data_1;
        float *hue_sat_map_data_2;
};

#define DCPTAG_COLOR_MATRIX_1 50721
#define DCPTAG_COLOR_MATRIX_2 50722
#define DCPTAG_PROFILE_HUE_SAT_MAP_DIMS 50937
#define DCPTAG_PROFILE_HUE_SAT_MAP_DATA_1 50938
#define DCPTAG_PROFILE_HUE_SAT_MAP_DATA_2 50939
#define DCPTAG_PROFILE_TONE_CURVE 50940
#define DCPTAG_CALIBRATION_ILLUMINANT_1 50778
#define DCPTAG_CALIBRATION_ILLUMINANT_2 50779
#define DCPTAG_FORWARD_MATRIX_1 50964
#define DCPTAG_FORWARD_MATRIX_2 50965

struct MPCameraCalibration parse_calibration_file(const char *path);
